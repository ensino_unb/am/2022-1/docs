# Plano da Disciplina - Aprendizado de Máquina (FGA0083)

## Professores
* Fabricio Ataides Braz

## Período
2º Semestre de 2.021

## Turma
1

## Ementa
* Introdução a métodos de aprendizado de máquina que são comumente utilizados em aplicações de reconhecimento de padrões em sinais (texto, som e imagem). 
* Regressão. 
* Classificação 
* Aprendizado não supervisionado. 
* Máquinas de vetores de suporte. 
* Redes neurais artificiais. 

## Método

Independente do método de ensino, a construção de modelos de Inteligencia Artificial envolve conhecimentos, cuja apreensão demanda experimentos contínuos de exercício das suas técnicas e fundamentos. Várias abordagens servem ao própósito de motivar o aluno a buscar esse conhecimento. Neste semestre decidimos aplicar o método de aprendizado baseado em projeto. 

Uma das principais mudanças que aprendizado baseado em projeto traz é que o foco sai da **instrução**, em que o professor em sala de aula instrui o aluno sobre conceitos e ferramentas, para a **investigação**, em que o aluno é desafiado a pesquisar conceitos, técnicas e ferramentas para conseguir alcançar os objetivos do projeto que ele se comprometeu a desenvolver. A perspectiva do professor muda da **instrução**, para a **tutoria**, no que diz respeito ao ensino. A perspectiva do aluno muda de **passiva** para **ativa**, no que diz respeito ao aprendizado.

A disciplina prevê um total de 60 horas de formação.

Serão formados grupos em que o aluno percorrerá uma trilha de aprendizagem voltada para a construção de app que embarque modelo de aprendizagem de máquina.

No que diz repeito a abordagem técnica para aprendizado de máquina, daremos preferência aos **modelos de aprendizagem profunda** (*deep learning*). Em razao disso, o tópico **redes neurais artificiais** será base para o ensino de modelagem supervisionada (classifição/regressão). Além disso, ao invés de modelos de maquina de suporte, o conteúdo da disciplina traz árvores de decisão.

## Ferramentas & Materiais
* [Teams](https://teams.microsoft.com/l/team/19%3arJpSDg3FqQg_6sp6CsWJNT9TA5EJ3ooPjVluOknCuhU1%40thread.tacv2/conversations?groupId=baa0faa1-ebf0-49d2-aae9-2849f04c722c&tenantId=ec359ba1-630b-4d2b-b833-c8e6d48f8059) - Comunicação e trabalho colaborativo;
* Python - Linguagem de programação;
* [Gitlab](https://gitlab.com/ensino_unb/am/2022-1) - Reposotório de código e coloboração;
* [Forum de Discussão](https://forum.ailab.unb.br)

## Avaliação

Para que o aluno seja aprovado na disciplina ele deve possuir desempenho superior ou igual a 50, correspondente a menção MM. Além disso, seu comparecimento deve ser superior ou igual a 75% dos eventos estabelecidos pela disciplina. 

### Desempenho

A avaliação de desempenho é resultado da avaliação pelo professor e convidado(s) do resultado do grupo, dado o projeto, juntamente com a avaliação individual do aluno pelos membros do grupo.

* AIP: avaliação individual de participação. A cada aula, o professor fará questionamentos diretos para os alunos sobre a lição da semana do fastai. Aquele que não conseguir responder perderá a pontuação associada a aula. 

* AGP: avaliação do grupo pelo professor. Esta avaliação acontece de acordo com os marcos estabelecidos no template de projeto do Trello compatilhado. No dia da apresentação será sorteado um membro do grupo que deverá ter a capacidade de responder pelo projeto.

* AIG: avaliação individual pelos membros do grupo.

![equation](https://latex.codecogs.com/gif.latex?%5Cfrac%7B%5Csum_%7B1%7D%5E%7Bn%7D%20AIP%7D%7B100%2An%7D%2A%280.4%2A%20%5Csqrt%7B%7BAGP_1%2AAIG_1%7D%7D%2B%200.6%2A%20%5Csqrt%7B%7BAGP_2%2AAIG_2%7D%7D)

Serão dois encontros avaliativos, observando os seguintes apectos do projeto:
1. Modelagem do problema e experimentos  (peso 0,4)
2. App que embarca modelo de ML (peso 0,6)

Além deles, a sua participação nos encontros de discussão sobre a lição do fastai também será objeto de avaliação. Esta, entretanto, servirá para credenciá-lo a participar da fase de projetos. Entendemos que se você não tiver se dedicado para as lições, não terá as condições míninas de ajudar o seu grupo para a relização de um projeto de valor.

| Aula | Data | Detalhe |
|---|---|---|
| 1 | 07/06 | Acolhida |
| 2 | 09/06 | lição 1 da fastai |
| 3 | 14/06 | **revisão da lição 1 da fastai** <br> lição 2 da fastai|
|  |  16/06 | Feriado |
| 4 | 21/06 | **revisão da lição 2 da fastai** |
| 5 | 23/06 | lição 3 da fastai |
| 6 | 28/06 | **revisão da lição 3 da fastai** |
| 7 | 30/06 | lição 4 da fastai |
| 8 | 05/07 | **revisão da lição 4 da fastai**  |
| 9 | 07/07 | lição 5 da fastai   |
| 10 | 12/07 | **revisão da lição 5 da fastai** |
| 11 | 14/07 | lição 6 da fastai   lição 5 da fastai |
| 12 | 19/07 |  **revisão da lição 6 da fastai**|
| 13 | 21/07 | lição 7 da fastai |
| 14 | 26/07 | **revisão da lição 7 da fastai** |
| 15 | 28/07 | lição 8 da fastai  |
| 16 | 02/08 | **revisão da lição 8 da fastai** |
| 17 | 04/08 | Definição dos Projetos |
| 18 | 09/08 | Projeto |
| 19 | 11/08 | Projeto |
| 20 | 16/08 | Projeto |
| 21 | 18/08 | Projeto |
| 22 | 23/08 | Avaliação 1 |
| 23 | 25/08 | Avaliação 1 |
||30/08| Semana Universitária|
||01/09| Semana Universitária|
| 24 | 06/09 | Projeto |
| 25 | 08/09 | Projeto|
| 26 | 13/09 | Projeto |
| 27 | 15/09 | Projeto |
| 28 | 20/09 | Avaliação 2 |
| 29 | 22/09 | Avaliação 2 |

### Comparecimento

As atividades síncronas serão consideradas para contabilizar a presença.

## Dinâmica no Grupo
Os grupos serão formados com a quantidade 6 alunos. Em cada iteração, ou seja, períodos entre os encontros avaliativos, deverão ser designados no grupo o `líder`, responsável por conduzir o time na execução das metas do período, além de reportar aos professores desvios e problemas de seus membros.

### Equilíbrio
Para que o grupo não seja prejudicado por eventuais desvios de seus membros, os alunos que não alcancarem nota igual ou superior a 5 na AIG serão desprezados do sorteio para as apresentações.

### Evasão
Grupos com menos de 4 alunos, terão seus membros distribuidos em outros grupos.

## Referências Bibliográficas

### Básica

* Kevin Patrick Murphy. Machine Learning: a Probabilistic Perspective Editor MIT press. 2012. Cambridge, MA.

* Chris Bishop. Pattern Recognition and Machine Learning. Editor Springer. 2006. New York.

* Ian Goodfellow, Yoshua Bengio, Aaron Courville. Deep Learning Editor MIT press. 2017. Cambridge, MA.

* [Yaser S. Abu-Mostafa, Malik Magdon-Ismail, Hsuan-Tien Lin. Learning from Data - a Short Course.](https://work.caltech.edu/telecourse.html). AML Book. 2012. Pasadena, CA. 

* Tom M. Mitchell. Machine Learning Editor. McGraw-Hill. 1997 

* David Barber. Bayesian Reasoning and Machine Learning. Cambridge University Press. 2012. Cambridge, UK. 

* Carl Edward Rasmussen, Christopher K. I. Williams. Gaussian Processes For Machine Learning Editor MIT press. 2006. Cambridge, MA 

* [Andrew Ng Local. Machine Learning Video Lectures](https://www.coursera.org/learn/machine-learning). Stanford, CA. 2014

### Complementar

* [Deep Learning with Pytorch](https://pytorch.org/assets/deep-learning/Deep-Learning-with-PyTorch.pdf)
* [Jeremy Howard and Sylvain Gugge. FastBook](https://github.com/fastai/fastbook)
* [AI Lab Forum](https://forum.ailab.unb.br)


